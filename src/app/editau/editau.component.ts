import { AuthorsService } from './../authors.service';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { VirtualTimeScheduler } from 'rxjs';

@Component({
  selector: 'app-editau',
  templateUrl: './editau.component.html',
  styleUrls: ['./editau.component.css']
})
export class EditauComponent implements OnInit {

  title:string;
  id:number;

  constructor(private router: Router, private authorsService:AuthorsService, private activatedRoute:ActivatedRoute) { }
  
  onSubmit(){
    this.router.navigate(['/authors', this.id,this.title]);
  }
  
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params.id;
  }

}
