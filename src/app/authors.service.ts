import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
  
  i=4;

  authors:any = [{id:1,name:'Lewis Carrol'},{id:2,name:'Leo Tolstoy'}, {id:3,name:'Thomas Mann'}]

  constructor() { }

  
  getauthors(){
    const authorsObservable = new Observable(
      observer =>{
      setInterval (()=> observer.next(this.authors),2000
        )
          }
        )
        return authorsObservable;
      } 

  addauthor(title:string){
    this.i++;
    this.authors.push({id:this.i,name:title});
  }
}
