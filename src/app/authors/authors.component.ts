import { AuthorsService } from './../authors.service';
import { Routes, Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  authors$:Observable<any>
  title:string;
  id:number;

  constructor(private authorsService:AuthorsService, private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.authors$ = this.authorsService.getauthors();
    this.id = this.activatedRoute.snapshot.params.id;
    this.title = this.activatedRoute.snapshot.params.title;
    if(this.id ==999){
      this.authorsService.addauthor(this.title);
    }
    
  }
}

