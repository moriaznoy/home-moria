import { AuthorsService } from './../authors.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addau',
  templateUrl: './addau.component.html',
  styleUrls: ['./addau.component.css']
})
export class AddauComponent implements OnInit {

  title:string; 

  constructor(private router:Router, private activatedRoute:ActivatedRoute, private authorsService:AuthorsService) { }

  onSubmit(){
    this.router.navigate(['/authors',999,this.title]);
  }

  ngOnInit() {
  }

}
