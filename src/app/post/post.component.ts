import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Post } from '../interface/post';
import { Userpost } from '../interface/userpost';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  posts$:Post[];
  users$:Userpost[];
  panelOpenState = false;
  title:string;
  body:string;
  author:string;

  constructor(private posts:PostsService) { }

  ngOnInit() {
    this.posts.getPost()
    .subscribe(data => this.posts$ = data);
    this.posts.getUser()
    .subscribe(data => this.users$ = data);

  }

  onSubmit(){
    for (let index = 0; index < this.posts$.length; index++) {
      for (let i = 0; i < this.users$.length; i++) {
        if (this.posts$[index].userId==this.users$[i].id) {
          this.title = this.posts$[index].title;
          this.body = this.posts$[index].body;
          this.author = this.users$[i].name;
          this.posts.addPosts(this.body,this.title,this.author);
        }             
      }     
    }
  }
}
