import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { FormsModule }   from '@angular/forms';
import { AuthorsComponent } from './authors/authors.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule, MatExpansionModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { EditauComponent } from './editau/editau.component';
import { AddauComponent } from './addau/addau.component';
import { PostComponent } from './post/post.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';




const appRoutes: Routes = [
  { path: 'authors', component: AuthorsComponent},
  { path: 'editauthors/:id', component: EditauComponent},
  { path: 'authors/:id/:title', component: AuthorsComponent},
  { path: 'Addauthor', component: AddauComponent},
  { path: 'post', component: PostComponent},
  { path: '',
  redirectTo: '/authors',
  pathMatch: 'full'
},
];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AuthorsComponent,
    EditauComponent,
    AddauComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    MatExpansionModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'home-moria'),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
