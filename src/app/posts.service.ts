import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './interface/post';
import { Userpost } from './interface/userpost';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private URL = "https://jsonplaceholder.typicode.com/posts/";
  private URL2 = "https://jsonplaceholder.typicode.com/users/";


  constructor(private http:HttpClient, private db:AngularFirestore ) { }

  getPost(){
    return this.http.get<Post[]>(this.URL);
  }

  getUser(){
    return this.http.get<Userpost[]>(this.URL2);
  }

  addPosts(body:string,title:string,author:string){ 
    const post = {body:body, title:title, author:author}; 
    this.db.collection('posts').add(post);
  }
}
