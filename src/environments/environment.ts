// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAH29xz8-mNKxLHwLdc5pPVs-sT9_y0RiY",
    authDomain: "home-moria-e2465.firebaseapp.com",
    databaseURL: "https://home-moria-e2465.firebaseio.com",
    projectId: "home-moria-e2465",
    storageBucket: "home-moria-e2465.appspot.com",
    messagingSenderId: "757025873868",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
